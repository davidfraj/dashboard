<?php
//Esto va a ser un ejemplo, para todo lo demas.

//Le digo la tabla de la base de datos
$tabla="clientes";

//Le digo los campos de la tabla
$campos=array('id_cliente', 
		'fecha_alta', 
		'titular',
		'cif_nif',
		'cups',
		'contacto',
		'n_cuenta',
		'telefono',
		'e_mail',
		'direccion',
		'poblacion',
		'cp',
		'observaciones');
//Le digo los tipos de campos
$tipos=array('numero', 
		'fecha', 
		'textocorto',
		'textocorto',
		'textocorto',
		'textocorto',
		'textocorto',
		'textocorto',
		'textocorto',
		'textocorto',
		'textocorto',
		'textocorto',
		'textolargo');
//Le decimos los titulos de los campos como saldran en la web.
$titulos=array('id', 
		'Fecha Alta', 
		'Particular/Empresa',
		'CIF/NIF',
		'CUPS',
		'Contacto/Administrador',
		'Número de Cuenta',
		'Teléfono',
		'Correo electrónico',
		'Dirección',
		'Población',
		'Código Postal',
		'Observaciones');

//Llamamos al CONSTRUCTOR DE LA CLASE Panel, y Creamos todo.
$panel=new Panel($tabla, $campos, $tipos, $titulos);
//Mediante el método (funcion) llamada accion, hacemos el resto.
$panel->accion();

?>