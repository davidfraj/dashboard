<?php
//Esto va a ser un ejemplo, para todo lo demas.

//Le digo la tabla de la base de datos
$tabla="tipo_comision";
//Le digo los campos de la tabla
$campos=array('id_tipocomision', 'nombre_tipocomision');
//Le digo los tipos de campos
$tipos=array('numero', 'textocorto');
//Le decimos los titulos de los campos como saldran en la web.
$titulos=array('id', 'Tipo de Comision');

//Llamamos al CONSTRUCTOR DE LA CLASE Panel, y Creamos todo.
$panel=new Panel($tabla, $campos, $tipos, $titulos);
//Mediante el método (funcion) llamada accion, hacemos el resto.
$panel->accion();

?>