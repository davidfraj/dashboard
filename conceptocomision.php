<?php
//Esto va a ser un ejemplo, para todo lo demas.

//Le digo la tabla de la base de datos
$tabla="concepto_comision";

//Vector con los datos, para crear el SELECT
$vector=array('id_compania', 'nombre_compania', 'companias');

//Le digo los campos de la tabla
$campos=array('id_conceptocomision', 'tarifa', 'potencia', 'consumo', 'importe', $vector, 'fecha', 'activado', 'observaciones');
//Le digo los tipos de campos
$tipos=array('numero', 'textocorto', 'textocorto', 'textocorto', 'textocorto', 'select', 'fecha', 'checkbox', 'textolargo');
//Le decimos los titulos de los campos como saldran en la web.
$titulos=array('id', 'Tipo de Tarifa', 'Tipo de Potencia', 'Tipo de Consumo', 'Importe', 'Compañia', 'Fecha alta', 'Activo', 'Observaciones');

//Llamamos al CONSTRUCTOR DE LA CLASE Panel, y Creamos todo.
$panel=new Panel($tabla, $campos, $tipos, $titulos);
//Mediante el método (funcion) llamada accion, hacemos el resto.
$panel->accion();

?>