<?php
//Esto va a ser un ejemplo, para todo lo demas.

//Le digo la tabla de la base de datos
$tabla="contratos";

//Vector con los datos, para crear el SELECT
		// id  +  lo que queremos presentar  +  nombre de la tabla de origen 
$vector=array('id_comercial', 'nombre_comercial', 'comerciales');
$vector2=array('id_comision', 'observaciones', 'comision');

$vector3=array('id_cliente', 'titular', 'clientes');

//Le digo los campos de la tabla
$campos=array('id_contrato', 
		'fecha_firma', 
		$vector3,
		$vector, 
		$vector2, 
			'consumo_anual',
		'importe_contrato', 
		'fecha_cobrado', 
		'fecha_pagado', 
		'observaciones');
//Le digo los tipos de campos
$tipos=array('numero', 
		'fecha', 
		'select',
		'select', 
		'select', 
			'textocorto',
		'decimal', 
		'fecha', 
		'fecha', 
		'textolargo');
//Le decimos los titulos de los campos como saldran en la web.
$titulos=array('id', 
		'Fecha Firma', 
		'Titular/Empresa',
		'Comercial', 
		'Comisión',
			'Consumo Anual', 
		'Importe', 
		'Cobrado', 
		'Pagado', 
		'Observaciones');

//Llamamos al CONSTRUCTOR DE LA CLASE Panel, y Creamos todo.
$panel=new Panel($tabla, $campos, $tipos, $titulos);
//Mediante el método (funcion) llamada accion, hacemos el resto.
$panel->accion();

?>