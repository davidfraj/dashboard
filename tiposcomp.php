<?php
//Esto va a ser un ejemplo, para todo lo demas.

//Le digo la tabla de la base de datos
$tabla="tipo_companias";
//Le digo los campos de la tabla
$campos=array('id_tipocomp', 'nombre_tipocomp');
//Le digo los tipos de campos
$tipos=array('numero', 'textocorto');
//Le decimos los titulos de los campos como saldran en la web.
$titulos=array('id', 'Tipo de Compañia');

//Llamamos al CONSTRUCTOR DE LA CLASE Panel, y Creamos todo.
$panel=new Panel($tabla, $campos, $tipos, $titulos);
//Mediante el método (funcion) llamada accion, hacemos el resto.
$panel->accion();

?>