<?php
//Esto va a ser un ejemplo, para todo lo demas.

//Le digo la tabla de la base de datos
$tabla="comisiones";
//Le digo los campos de la tabla
$campos=array('id_comisiones', 'id_conceptocomision', 'id_comision', 'nombre');
//Le digo los tipos de campos
$tipos=array('numero', 'textocorto', 'textocorto', 'textocorto');
//Le decimos los titulos de los campos como saldran en la web.
$titulos=array('id', 'Concepto Comisión', 'Comisión', 'Nombre');

//Llamamos al CONSTRUCTOR DE LA CLASE Panel, y Creamos todo.
$panel=new Panel($tabla, $campos, $tipos, $titulos);
//Mediante el método (funcion) llamada accion, hacemos el resto.
$panel->accion();

?>