<?php
/* Funciones variadas */

function fechaCopyright($inicio){
	$res=$inicio;
	if(date('Y')>$inicio){
		$res.=' - '.date('Y');
	}
	return $res;
}

// Para visualiar en tabla los resultados de un SELECT

function tabla_out($result)  {
		// En la variable $cant almacenamos la cantidad de campos que vamos a mostrar
		$cant = mysqli_num_fields($result);
	echo "<table>";
		echo "<tr>";
			// Bucle para cargar los registros de datos
			for ($i=0; $i < $cant ; $i++) { 
				echo "<th>";
						// Función que proporciona un registro de datos del comando SELECT
					echo mysql_field_name($result, $i);
				echo "</th>";
			}
		echo "</tr>";
		
		echo "<tr>";
			$num = mysqli_num_rows($result);
				for ($j=0; $j < $num; $j++) { 
					$rows = mysql_fetch_array($result);
					echo "<tr>";
						for ($k=0; $k < $cant; $k++) { 
							$fn = mysql_field_name($result, $k);
							echo "<td>" . '$row[$fn]' . "</td>";
						}
					echo "</tr>";				
				echo "</tr>";
				}
	echo "</table>";
}
/* EXPLICACION:
En la variable $cant se almacena la cantidad de campos que deben mostrarse, y ello se consigue con ayuda de la 
función mysql_num_rows. En un bucle se cargan los tídulos de las tablas.
La función mysql_field_name proporciona un registro de datos del comando SELECT. Por otra parte, los registros de datos 
también se cargan en un bucle.
Vemos la aplicación de esta función con un ejemplo en un supuesto "archivo.php". Aquí se muestran datos de una supuesta
"tabla_01". La orden SELECT se almacena en la variable $sql. Si la consulta devuelve resultados, éstos se visualizan con 
el comando "echo", y los resultados se transmiten a nuestra función "tab_out()".
	EJEMPLO:	
	<?php  //archivo.php
	include("conexion.php");
	include("func.php");

	$sql = "SELECT * FROM tabla_01";
	If ($res = send_sql($db,$sql))  {
		echo "Consulta: <br> $sql";
	}
	tab_out($res);
	?>
*/

?>