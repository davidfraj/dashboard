<?php
//Esto va a ser un ejemplo, para todo lo demas.

//Le digo la tabla de la base de datos
$tabla="comision";

//Vector con los datos, para crear el SELECT
$vector=array('id_tipocomision', 'nombre_tipocomision', 'tipo_comision');
$vector2=array('id_conceptocomision', 'observaciones', 'concepto_comision');

//Le digo los campos de la tabla
$campos=array('id_comision', $vector, $vector2, 'importe_comision', 'observaciones');
//Le digo los tipos de campos
$tipos=array('numero', 'select', 'select', 'decimal', 'textolargo');
//Le decimos los titulos de los campos como saldran en la web.
$titulos=array('id', 'Tipo de Comisión', 'Concepto Comisión', 'Importe Comisión', 'Observaciones');

//Llamamos al CONSTRUCTOR DE LA CLASE Panel, y Creamos todo.
$panel=new Panel($tabla, $campos, $tipos, $titulos);
//Mediante el método (funcion) llamada accion, hacemos el resto.
$panel->accion();

?>