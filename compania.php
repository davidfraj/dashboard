<?php
//Esto va a ser un ejemplo, para todo lo demas.

//Le digo la tabla de la base de datos
$tabla="companias";

//Vector con los datos, para crear el SELECT
$vector=array('id_tipocomp', 'nombre_tipocomp', 'tipo_companias');

//Le digo los campos de la tabla
$campos=array('id_compania', 'nombre_compania', $vector, 'cif_compania', 'direccion_compania', 'tel_compania', 'correo_compania', 'contacto_compania', 'poblacion_compania', 'cp_compania', 'alta_compania', 'compania_activo', 'observaciones');
//Le digo los tipos de campos
$tipos=array('numero', 'textocorto', 'select', 'textocorto', 'textocorto', 'textocorto','textocorto', 'textocorto', 'textocorto', 'textocorto', 'fecha', 'checkbox', 'textocorto');
//Le decimos los titulos de los campos como saldran en la web.
$titulos=array('id', 'Nombre Compañia', 'Tipo Compañia', 'CIF', 'Dirección', 'Teléfono', 'Correo Electrónico', 'Contacto', 'Población', 'Código Postal', 'Fecha de alta', 'Activado', 'Observaciones');

//Llamamos al CONSTRUCTOR DE LA CLASE '"PanelComerciales"', y Creamos todo.
$panel=new Panel($tabla, $campos, $tipos, $titulos);
//Mediante el método (funcion) llamada accion, hacemos el resto.
$panel->accion();

?>