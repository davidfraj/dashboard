<?php
include('includes/conexion.php');
include('includes/menu.class.php');
include('includes/panel.class.php');
include('includes/func.php');

if(isset($_GET['p'])){
	$p=$_GET['p'];
}else{
	$p='inicio.php';
}

?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>DashBoard - Panel de control</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <link rel="stylesheet" href="css/datepicker.css" >
    <link rel="stylesheet" href="css/estilos.css" >

    <link rel="icon" type="image/png" href="img/ARAmarketing-favicon.png" /> 
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container">
        <div class="page-header">
          <img src="img/ARAmarketing-logo_353x175.png" width="13%">
        <h3>PANEL DE ADMINISTRACIÓN - <small>Gestion de elementos</small></h3>
      </div>
		<br>
    <?php
    //Llamamos al menu de la parte superior
    $titulos=array('Inicio', 'Tipos Compañia', 'Tipos Comisión', 'Compañia', 'Comisión', 'Concepto Comisión', 'Contratos', 'Clientes');
    $archivos=array('inicio.php', 'tiposcomp.php', 'tiposcomision.php', 'compania.php', 'comision.php', 'conceptocomision.php', 'contratos.php', 'clientes.php');
    $menu=new menu($titulos, $archivos);
    $menu->mostrar();

    ?>

		<div class="container">
			<br>
      <br>
			<?php
				include($p);
			?>
	  </div>
    

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="js/locales/bootstrap-datepicker.es.js"></script>
    <script type="text/javascript">
      $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        language: 'es'
      });
    </script>
  </body>
  <footer>
    <h5>Cualquier duda o problema: <a href="">informatica@davidfraj.com</a></h5>
   
    <p>&copy; <?php echo fechaCopyright(2015);?> | davidfraj.com Marketing | Zaragoza
        <br>Versión 2.1</p>
  </footer>
  </div>
</html>
<?php
$conexion->close();
?>